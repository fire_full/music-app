import {v4 as uuidv4} from 'uuid';


const childHod =() => {
    return  [
        {
            name :"Sleepy Fish",
            artist:"Lofi Hip Hop",
            cover:"https://chillhop.com/wp-content/uploads/2021/09/2ce75252f5419a45d76bb93424ac1eae3e688b17-1024x1024.jpg",
            id:uuidv4(),
            active : true, 
            color : ["#BA4A46", '#F00'],
            audio:"https://mp3.chillhop.com/serve.php/?mp3=23057", 
        },
        {
            name :"Lagoons",
            artist:"Strehlow, Chris Mazuera",
            cover:"https://chillhop.com/wp-content/uploads/2021/08/b0bb2309d0c8fe0a32907ecddab689501b7de5cf-1024x1024.jpg",
            id:uuidv4(),
            active : false, 
            color : ["#BA4A46", '#F00'],
            audio:"https://mp3.chillhop.com/serve.php/?mp3=24642", 
        },
        {
            name :"La Zona",
            artist:"Maydee",
            cover:"https://i.scdn.co/image/ab67616d0000b273e2438fdc3991b884b15b6f8e",
            id:uuidv4(),
            active : false, 
            color : ["#BA4A46", '#F00'],
            audio:"https://mp3.chillhop.com/serve.php/?mp3=27455", 
        }
        ,
        {
            name :"Snaring River",
            artist:"Moods",
            cover:"https://i.scdn.co/image/ab67616d0000b273ed624e07e45841dc5680ed05",
            id:uuidv4(),
            active : false, 
            color : ["#BA4A46", '#F00'],
            audio:"https://mp3.chillhop.com/serve.php/?mp3=24694", 
        }
        ,
        {
            name :"Happy In My Mind",
            artist:"Otexhello",
            cover:"https://i.scdn.co/image/ab67616d0000b273eb84407e80d5762051c88fed",
            id:uuidv4(),
            active : false, 
            color : ["#BA4A46", '#F00'],
            audio:"https://mp3.chillhop.com/serve.php/?mp3=24701", 
        },
        {
            name :"The Little Place",
            artist:"Taro",
            cover:"https://i.scdn.co/image/ab67616d0000b273eb84407e80d5762051c88fed",
            id:uuidv4(),
            active : false, 
            color : ["#BA4A46", '#F00'],
            audio:"https://mp3.chillhop.com/serve.php/?mp3=24698", 
        },
        {
            name :"Mortal Wombat",
            artist:"Toonorth",
            cover:"https://i.scdn.co/image/ab67616d0000b2734e1a0bafd800b197475e004d",
            id:uuidv4(),
            active : false, 
            color : ["#BA4A46", '#F00'],
            audio:"https://mp3.chillhop.com/serve.php/?mp3=24711", 
        },
        {
            name :"Woozii",
            artist:"Invention",
            cover:"https://i.scdn.co/image/ab67616d0000b27376a566b87eb85612860721ff",
            id:uuidv4(),
            active : false, 
            color : ["#BA4A46", '#F00'],
            audio:"https://mp3.chillhop.com/serve.php/?mp3=24705", 
        },
        {
            name :"Warmath",
            artist:"Strehlow",
            cover:"https://i.scdn.co/image/ab67616d0000b27382d86cde91ad9e5919c04327",
            id:uuidv4(),
            active : false, 
            color : ["#BA4A46", '#F00'],
            audio:"https://mp3.chillhop.com/serve.php/?mp3=24710", 
        },
        {
            name :"Polaroid",
            artist:"Makozo",
            cover:"https://i.scdn.co/image/ab67616d0000b273e0388f7a99ee3009d8e22128",
            id:uuidv4(),
            active : false, 
            color : ["#BA4A46", '#F00'],
            audio:"https://mp3.chillhop.com/serve.php/?mp3=24767", 
        },
        {
            name :"Banksy",
            artist:"Masked Man",
            cover:"https://i.scdn.co/image/ab67616d0000b2736c56565c18b0a012e5a744f6",
            id:uuidv4(),
            active : false, 
            color : ["#BA4A46", '#F00'],
            audio:"https://mp3.chillhop.com/serve.php/?mp3=24686", 
        }
        
    ]
}

export default childHod