import React,{useState ,useRef,useEffect} from 'react';
import Song from './Components/Song';
import Player from './Components/Player';
import data from './data'
import {SimpleData , Idata} from './Models/data'
import Library from './Components/Library';
import {ISongInfo} from './Models/data'
import Nav from './Components/Nav'
import {useLocalStorage} from './hooks/useLocalStorage'

const defaultToggle = false

function App() {
  const [songs ,setSongs] = useState<SimpleData>(data())
  const [currentSongs ,setCurrentSongs] = useState<Idata>(songs[0])
  const [isPlaying ,setIsPlaying] =useState<boolean>(false)
  const [songInfo, setSongInfo] = useState<ISongInfo>();
  const audioRef: any = useRef(null);

 

  // const [isOpenLibrary ,setIsOpenLibrary] = useState<boolean>(false)

  const [ isOpenLibrary, setIsOpenLibrary ] = useLocalStorage('toggle', defaultToggle);

  const timeHandler = (e: any) => {
    const current: any = e.target.currentTime;
    const duration: any = e.target.duration;
    const roundedCurrent = Math.round(current)
    const roundedDuration = Math.round(duration)
    const animation = Math.round((roundedCurrent / roundedDuration) * 100)
    console.log('animation',animation)
    setSongInfo({ ...songInfo, currentTime: current, duration: duration , animationPercentage: animation  });
    console.log("time handler ", current, duration);
  }

  useEffect(
		() => {
			typeof isOpenLibrary !== typeof undefined && setIsOpenLibrary(JSON.parse(localStorage.getItem('toggle')!))
		},[isOpenLibrary , setIsOpenLibrary])

    const songEndHanler = async () =>{
      const currentIndex = songs.findIndex((song : Idata) => song.id === currentSongs.id)
      await setCurrentSongs(songs[(currentIndex + 1) % songs.length])

      if(isPlaying) audioRef.current.play()
    }

  return (
    <div className={`App ${isOpenLibrary ? 'library-active'  : ''}`}>
      <Nav 
      isOpenLibrary={isOpenLibrary} 
      setIsOpenLibrary={setIsOpenLibrary}
      />
      <Song currentSongs={currentSongs} />
      <Player 
        setSongInfo={setSongInfo}
        songInfo={songInfo}
        audioRef={audioRef}
        setIsPlaying={setIsPlaying}
        isPlaying={isPlaying}
        currentSongs={currentSongs} 
        songs={songs}
        setCurrentSongs={setCurrentSongs}
        setSongs={setSongs}
      />
      <Library 
       setIsOpenLibrary={setIsOpenLibrary}
       isOpenLibrary={isOpenLibrary}
       songs={songs} 
       setCurrentSongs={setCurrentSongs}
       audioRef={audioRef}
       isPlaying={isPlaying}
       setSongs={setSongs}
       />

      <audio
        ref={audioRef}
        onTimeUpdate={timeHandler}
        onLoadedMetadata={timeHandler}
        onLoadedData={timeHandler}
        src={currentSongs?.audio}
        onEnded={songEndHanler}
      ></audio>
    </div>
  );
}

export default App;
