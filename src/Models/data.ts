export interface Idata{
    name : string
    artist: string
    cover : string
    id  :string
    active : boolean
    color: string[]
    audio : string
}

export type SimpleData = Idata[]


export interface ISongInfo {
    currentTime: number;
    duration: number;
    animationPercentage : number

  }