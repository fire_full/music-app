import React from 'react'
import Librarysong from './Librarysong'
import {SimpleData , Idata} from './../Models/data'
import {AiOutlineCloseCircle} from 'react-icons/ai'
interface IData {
    setCurrentSongs :React.Dispatch<React.SetStateAction<Idata>>
    songs : SimpleData
    audioRef : any
    isPlaying : boolean
    setSongs : React.Dispatch<React.SetStateAction<SimpleData>>
    isOpenLibrary: boolean
    setIsOpenLibrary : any
}

 const Library : React.FC<IData> = ({songs , setCurrentSongs ,setIsOpenLibrary, audioRef ,isPlaying ,setSongs ,isOpenLibrary}) => {
    return (
        <div className={`library ${isOpenLibrary ? 'active-library' : ''}`}>
            <div style={{display:'flex' ,  flexDirection:'row' , alignItems:'center' , justifyContent:'space-between', padding:'5px'}}>
                <h2>Library</h2>
                <span style={{width:'40px' , height:'40px' , cursor:'pointer'}} onClick={() => setIsOpenLibrary(!setIsOpenLibrary)}>
                    <AiOutlineCloseCircle style={{width:'20px' , height:'20px' , cursor:'pointer'}}/>
                </span>
            </div>
            <div className="library-songs">
                
                {songs.map((song :Idata ) => 
                    <Librarysong 
                     songs={songs}
                     song={song} 
                     id={song?.id}
                     setCurrentSongs={setCurrentSongs}
                     key={song.id}
                     audioRef={audioRef}
                     isPlaying={isPlaying}
                     setSongs={setSongs}
                     />
                )}
            </div>
        </div>
    )
}
export default Library
