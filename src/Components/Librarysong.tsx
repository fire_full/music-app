import React from 'react'
import {Idata ,SimpleData} from '../Models/data'


interface PropsModel {
    song : Idata
    setCurrentSongs :React.Dispatch<React.SetStateAction<Idata>>
    songs : SimpleData
    id : string
    audioRef: any
    isPlaying : boolean
    setSongs : React.Dispatch<React.SetStateAction<SimpleData>>
}

 const Librarysong : React.FC<PropsModel> = ({song , setCurrentSongs , id , songs , audioRef ,isPlaying ,setSongs})=>{

    const songSelectHandle = ()=>{
        const selectedsong = songs.filter((state : Idata) => state?.id === id)

        setCurrentSongs(selectedsong[0])

        const newSongs :any = songs.map((song : Idata) => {
            if(song.id === id){
                return {
                    ...song ,
                    active : true
                }
            }else {
                return {
                    ...song ,
                    active : false
                }
            }
        })
        setSongs(newSongs)

        if(isPlaying) audioRef.current.play()
    }

    
    return (
        <div onClick={songSelectHandle} className={`library-song ${song?.active ? 'selected' : ''}`  }>
            <img src={song?.cover} alt={song?.name}/>
            <div className="song-desc">
                <h3>{song?.name}</h3>
                <h4>{song?.artist}</h4>
            </div>
        </div>
    )
}
export default Librarysong