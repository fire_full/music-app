import React from 'react'
import {Idata} from '../Models/data'

interface PropsModel {
    currentSongs : Idata
}

 const Song : React.FC<PropsModel> = ({currentSongs})=>{
    return (
        <div className="song-container">
            <img src={currentSongs?.cover} alt={currentSongs?.name}/>
            <h1>{currentSongs?.name}</h1>
            <h2>{currentSongs?.artist}</h2>
        </div>
    )
}
export default Song