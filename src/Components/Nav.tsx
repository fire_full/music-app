import React  from 'react'
import {BsMusicNoteBeamed} from 'react-icons/bs'


interface INavProps {
    setIsOpenLibrary : React.Dispatch<React.SetStateAction<boolean>>
    isOpenLibrary : boolean
}

 const Nav : React.FC<INavProps> = ({setIsOpenLibrary , isOpenLibrary}) => {

    const onChangeToggle = () => {
		setIsOpenLibrary((isOpenLibrary) => !isOpenLibrary);
	};

   
    return (
        <nav>
            <h1>
                Waves
            </h1>
            <button onClick={() => onChangeToggle()}>
                <h4 className="font">Library</h4>
                <BsMusicNoteBeamed className="forIcon"/>
            </button>            
        </nav>
    )
}
export default Nav
