import React,{CSSProperties} from "react"; 
import { AiOutlinePlayCircle } from "react-icons/ai";
import { BsSkipBackwardCircle } from "react-icons/bs";
import { BsSkipForwardCircle } from "react-icons/bs";
import { Idata , SimpleData} from "../Models/data";
import { AiOutlinePauseCircle } from "react-icons/ai";
import {ISongInfo} from './../Models/data'

interface PropsModel {
  currentSongs: Idata;
  setIsPlaying: React.Dispatch<React.SetStateAction<boolean>>;
  isPlaying: boolean;
  audioRef : any
  setSongInfo: React.Dispatch<React.SetStateAction<ISongInfo | undefined>>
  songInfo: ISongInfo | undefined
  songs : SimpleData
  setCurrentSongs: React.Dispatch<React.SetStateAction<Idata>>
  setSongs:React.Dispatch<React.SetStateAction<SimpleData>>
}


const Player: React.FC<PropsModel> = ({
  currentSongs,
  isPlaying, 
  setIsPlaying,
  audioRef,
  setSongInfo ,
  songInfo,
  songs,
  setCurrentSongs,
  setSongs
}) => {

  const activeLibraryHandler = (nextPerv :any) =>{
    const newSongs :any = songs.map((song : Idata) => {
      if(song?.id === nextPerv?.id){
          return {
              ...song ,
              active : true
          }
      }else {
          return {
              ...song ,
              active : false
          }
      }
    })
    setSongs(newSongs)

    if(isPlaying) audioRef.current.play()
  }
  

  const playSongHandler = () => {
    if (isPlaying) {
      audioRef?.current?.pause(); 
      setIsPlaying(!isPlaying);
    } else {
      audioRef?.current?.play();
      setIsPlaying(!isPlaying);
    }
  };


  const onDragHandler = (e : any) =>{
      audioRef.current.currentTime = e.target.value
      setSongInfo({...songInfo! , currentTime : e.target.value })
  }

  const getTime = (time: number) => {
    return (Math.floor(time / 60) + ":" + ("0" + Math.floor(time % 60)).slice(-2))
  }

  const skipTrackHandler = async (direction : string) =>{
    
    const currentIndex = songs.findIndex((song : Idata) => song.id === currentSongs.id)
    if(direction === 'skip-forward'){ 
      await setCurrentSongs(songs[(currentIndex + 1) % songs.length])
      activeLibraryHandler(songs[(currentIndex + 1) % songs.length])
    }
    if(direction === 'skip-track'){
      if((currentIndex - 1 ) % songs.length === -1){
        await setCurrentSongs(songs[songs.length - 1 ])
        activeLibraryHandler(songs[songs.length - 1 ])
        if(isPlaying) audioRef.current.play()
        return
      }
        await setCurrentSongs(songs[(currentIndex - 1) % songs.length])
        activeLibraryHandler(songs[(currentIndex - 1) % songs.length])
    }
    if(isPlaying) audioRef.current.play()
  }





  //add styles 
  const trackAnim : CSSProperties = {
     transform  : `translateX(${songInfo?.animationPercentage}%)`,

  }
  return (
    <div className="player">
      <div className="time-control">
        <p>
          {isNaN(songInfo?.currentTime!)
            ? "0:00"
            : getTime(songInfo?.currentTime!)}{" "}
        </p>
        
        <div className="track"
             style={{background: `linear-gradient(to right ,${currentSongs?.color[0]}, ${currentSongs?.color[1]} )`}}
        >

          <input 
          min={0}
          max={songInfo?.duration!}
          value={songInfo?.currentTime!}
          // value={dataUpdate}
          onChange={onDragHandler}
          type="range" 
          />

          <div style={trackAnim} className="animate-track"></div>
        </div>

        <p>
          {isNaN(songInfo?.duration!) ? "0:00" : getTime(songInfo?.duration!)}
        </p>
      </div>
      <div className="paly-control">
        <BsSkipBackwardCircle className="ForandBack" onClick={() => skipTrackHandler('skip-track')}/>

        {isPlaying === false ? (
          <AiOutlinePlayCircle onClick={playSongHandler} className="palyIcon" />
        ) : (
          <AiOutlinePauseCircle
            onClick={playSongHandler}
            className="palyIcon"
          />
        )}
        <BsSkipForwardCircle className="ForandBack" onClick={() => skipTrackHandler('skip-forward')}/>
      </div>
      {/* <audio
        ref={audioRef}
        onTimeUpdate={timeHandler}
        onLoadedMetadata={timeHandler}
        src={currentSongs.audio}
      ></audio> */}
    </div>
  );
};
export default Player;
